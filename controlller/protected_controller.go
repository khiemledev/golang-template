package controller

import (
	"fmt"
	"golang_template/helper"
	"golang_template/middleware"
	"net/http"

	"github.com/gin-gonic/gin"
)

type ProtectedController struct{}

func (ctrl *ProtectedController) Protected_endpoint(ctx *gin.Context) {
	authPayload := ctx.MustGet(middleware.AuthorizationPayloadKey)

	fmt.Println(authPayload)

	helper.NewHTTPResponse(
		ctx,
		http.StatusOK,
		nil,
	)
}
