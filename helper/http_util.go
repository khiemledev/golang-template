package helper

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func NewHTTPError(ctx *gin.Context, status int, err error) {
	er := HTTPResponse{
		StatusCode: status,
		Message:    err.Error(),
	}
	ctx.AbortWithStatusJSON(status, er)
}

func NewHTTPResponse(ctx *gin.Context, status int, data interface{}) {
	rsp := HTTPResponse{
		StatusCode: status,
		Message:    http.StatusText(status),
		Data:       data,
	}
	ctx.JSON(status, rsp)
}

type HTTPResponse struct {
	StatusCode int         `json:"status_code" example:"400"`
	Message    string      `json:"message" example:"status bad request"`
	Data       interface{} `json:"data,omitempty"`
}
